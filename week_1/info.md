### **Неделя 1 - Php работа со строками, массивами, функциями:**


### **Цель:**



1. строки:
    *   форматирование
    *   склейка
    *   разбивка
    *   поиск и замена
2. работа с массивами:
    *   создание 
    *   обход
    *   слияние
    *   поиск
    *   фильтрация


### **Литература:**


### Офф дока:


*   [Основы синтаксиса](http://php.net/manual/ru/language.basic-syntax.php)
*   [Типы](http://php.net/manual/ru/language.types.php)
*   [Переменные](http://php.net/manual/ru/language.variables.php)
*   [Константы](http://php.net/manual/ru/language.constants.php)
*   [Выражения](http://php.net/manual/ru/language.expressions.php)
*   [Операторы](http://php.net/manual/ru/language.operators.php)
*   [Управляющие конструкции](http://php.net/manual/ru/language.control-structures.php)
*   [Функции](http://php.net/manual/ru/language.functions.php)
*   [Строковые функции](https://php.net/manual/ru/book.strings.php)
*   [Функции работы с массивами](https://php.net/manual/ru/book.array.php)
*   [Функции управления функциями](https://php.net/manual/ru/book.funchand.php)
*   [SOLID](https://medium.com/webbdev/solid-4ffc018077da)
*   [Многоуровневая архитектура](https://ru.wikipedia.org/wiki/%D0%9C%D0%BD%D0%BE%D0%B3%D0%BE%D1%83%D1%80%D0%BE%D0%B2%D0%BD%D0%B5%D0%B2%D0%B0%D1%8F_%D0%B0%D1%80%D1%85%D0%B8%D1%82%D0%B5%D0%BA%D1%82%D1%83%D1%80%D0%B0)

Более подробно смотреть те же темы тут(все изучать не нужно, выбираем что-то одно что понятней):  \
	Книга PHP7 в подлинике, главы - 6,7,9,10,11,13,14,15 \
	[https://www.w3schools.com/php7/](https://www.w3schools.com/php7/)  \
	[https://phptherightway.com/pages/The-Basics.html](https://phptherightway.com/pages/The-Basics.html) \
	https://www.udemy.com/code-dynamic-websites/

Об оформлении кода: \
	[https://phptherightway.com/#code_style_guide](https://phptherightway.com/#code_style_guide)
