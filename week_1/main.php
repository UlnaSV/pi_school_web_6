<?php
    $data = <<< TMAKR
    Article:
        Header: BMW AG
        Body: Bayerische Motoren Werk is a German multinational company which currently produces automobiles and motorcycles, and also produced aircraft engines until 1945. The history of the name itself begins with Rapp Motorenwerke, an aircraft engine manufacturer. In April 1917, following the departure of the founder Karl Friedrich Rapp, the company was renamed Bayerische Motoren Werk
        ChangeMap:
            Bayerische Motoren Werk: Bavarian Motor Works(BMW)
        Tags: Automotive Industry, Germany, Luxury
    Article:
        Header: Volkswagen
        Body:  Volkswagen is a German automaker founded on 28 May 1937 by the German Labour Front, and headquartered in Wolfsburg. Volkswagen was established in 1937 by the German Labour Front in Berlin.
        ChangeMap:
            Volkswagen: VW
            German: GR
        Tags: Automotive Industry, Germany, Not-Luxury
    Article:
        Header: Project E
        Body: Project E was a joint project between the United States and the United Kingdom during the Cold War to provide nuclear weapons to the Royal Air Force (RAF) until sufficient British nuclear weapons became available. It was subsequently expanded to provide similar arrangements for the British Army of the Rhine
        ChangeMap:
            Project E: P.E.
        Tags: Military Industry, USA
    Article:
        Header: Ford Motor Company
        Body: Ford Motor Company is a multinational automaker that has its main headquarter in Dearborn, Michigan, a suburb of Detroit. It was founded by Henry Ford and incorporated on June 16, 1903.
        ChangeMap:
            Ford Motor Company: Ford
        Tags: Automotive Industry, USA, Not-Luxury
    Article:
        Header: Studebaker US6
        Body: The Studebaker US6 (G630) was a series of 2½-ton 6x6 and 5-ton 6x4 trucks manufactured by the Studebaker Corporation and REO Motor Car Company during World War II. The basic cargo version was designed to transport a 5,000 lb (2,300 kg) cargo load over all types of terrain in all kinds of weather.
        Tags: Heavy Automotive Industry, USA, Not-Luxury
    TMAKR;
    
    function addSeparator($data) {
        $data = explode(PHP_EOL, $data);
        $symbol = ':';
        for ($i = 0; $i < count($data); $i++) {
            $data[$i] = substr($data[$i], -1) == $symbol ? $data[$i] : $data[$i] . $symbol;;
        }
        $data = implode($data);
        return $data;
    }


    function dataTomassive($data) {  //convert $data to array. Accepts string.
        $data = addSeparator($data);
        $arrayOfData = explode(':', $data);

        return $arrayOfData;
    }


    function findOverlaps($data) { //find indexes of coincidences
        $dataArray = dataTomassive($data);
        $counter = 0;
        $counted = [];

        for ($i = 0; $i < count($dataArray); $i++) {
            if (substr($dataArray[$i], 1, 19) == 'Automotive Industry') {
                $counter = $counter + 1;
                array_push($counted, $i);
            }
        }
        return $counted;
    }
    

    function findHeaders($data) {
        $headers = [];
        $overlaps = findOverlaps($data);
     
        for ($j = 0; $j < 3; $j++) {
            for ($i = $overlaps[$j]; $i > $overlaps[$j] - 11; $i--) {
                if (dataTomassive($data)[$i] == dataTomassive($data)[1]) {
                    array_push($headers,dataTomassive($data)[$i+1]);
                    break;
                }
            }
            if (count($headers) == count($overlaps)) {
                break;
            }
        }       
        return $headers;
    }


    function findBodies($data) {
        $bodies = [];
        $overlaps = findOverlaps($data);

        for ($j = 0; $j < 3; $j++) {
            for ($i = $overlaps[$j]; $i > $overlaps[$j] - 11; $i--) {
                if (dataTomassive($data)[$i] == dataTomassive($data)[3]) {
                    array_push($bodies,dataTomassive($data)[$i+1]);
                    break;
                }
            }
            if (count($bodies) == count($overlaps)) {
                break;
            }
        }    

        
        return $bodies;
    }

   function modifyBodies($data) {
       $bodies = findBodies($data);
       $overlaps = findOverlaps($data);

       for ($j = 0; $j < 3; $j++) {
        for ($i = $overlaps[$j]; $i > $overlaps[$j] - 11; $i--) {
            if (dataTomassive($data)[$i] == dataTomassive($data)[5]) {
                for ($k = 1; $k < $overlaps[$j] - 1 - $i; $k = $k + 2) {
                    $bodies[$j] = str_replace(ltrim(dataTomassive($data)[$i+$k]), dataTomassive($data)[$i+$k + 1] ,$bodies[$j]);
                }
                break;
            }
        }   
    }  
    return $bodies;  
   }



   function findTags($data) {
       $overlaps = findOverlaps($data);
       $tags = [];

       for ($i = 0; $i < count($overlaps); $i++) {
           array_push($tags, dataTomassive($data)[$overlaps[$i]]);
       }
       return $tags;
   }

    
    function createTableOfContent($data) {
        $arrayOfHeaders = findHeaders($data);
        echo "<html>
<body>";
        echo PHP_EOL;
        echo "<table-of-content>";
        echo PHP_EOL;
        for ($i = 0; $i < count($arrayOfHeaders); $i++) {
            echo "<div>{$arrayOfHeaders[$i]}</div>";
        }
        echo PHP_EOL;
        echo "</table-of-content>";
        echo PHP_EOL;
    }
    
    function createContent($data) {
        $arrayOfHeaders = findHeaders($data);
        $arrayOfBodies = modifyBodies($data);
        echo "<content>";
        echo PHP_EOL;
        for ($i = 0; $i < count($arrayOfHeaders); $i++) {
            echo "<article>
            <h1>{$arrayOfHeaders[$i]}</h1>
            <p>{$arrayOfBodies[$i]}</p>
            </article>
            ";
        }
        echo PHP_EOL;
        echo "</content>";
        echo PHP_EOL;
    }
//    $s = implode(",", findTags($data));
//    $s1 = explode(",", $s);
//    $s2 = array_unique($s1);
//    foreach ($s2 as $el) {
//        print $el;
//        print ",";
//    }
    
    function createTags($data) {
        $tags = findTags($data);
        $tags = implode(",",$tags);
        $tags = explode(",", $tags);
        $tags = array_unique($tags);

        echo "<tags>";
        echo PHP_EOL;
        foreach($tags as $tag) {
            echo $tag;
            echo ".";
            echo PHP_EOL;
        }
        echo "</tags>";
        echo PHP_EOL;
        echo "</body>
</html>";
    }

    createTableOfContent($data);
    createContent($data);
    createTags($data);
?>
